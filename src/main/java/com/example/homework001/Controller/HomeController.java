package com.example.homework001.Controller;
import com.example.homework001.Model.Information;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    List<Information> informationList = new ArrayList<>();
    {
        informationList.add(new Information(1,"Java","Java is programming Langauge "));
        informationList.add(new Information(2,"Introduction", "My name is Join. I'm 22 years old"));
        informationList.add(new Information(3,"HTML", "Hypertext Markup Manguage"));
    }
    @GetMapping("/")
    public String myPage(ModelMap modelMap){
        modelMap.addAttribute("informationList",informationList);
        return "mypage";
    }

    @GetMapping("/create-new")
    public String createPage(){
        // @ModelAttribute Information information, ModelMap modelMap //pass agurment
        //modelMap.addAttribute("information", information);
    return "createPage";
    }

    @PostMapping("/add-new")
    public String insertInfo(@ModelAttribute Information information){
        information.setId(informationList.size()+1);
        informationList.add(information);
        return "redirect:/";
    }
}
